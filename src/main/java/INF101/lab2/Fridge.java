package INF101.lab2;

import java.time.LocalDate;
import java.util.*;

public class Fridge implements IFridge{

    private final int maxSize = 20;
    List<FridgeItem> items = new ArrayList();

    @Override
    public int nItemsInFridge() {
        return items.size() ;
    }

    @Override
    public int totalSize() {
        return maxSize;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        items.add(item);
        if (items.size() < maxSize) {
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (!items.contains(item)) {
            throw new NoSuchElementException();
        }
        items.remove(item);
        }


    @Override
    public void emptyFridge() {
        items.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredItems = new ArrayList();
        for (FridgeItem i : items) {
            //System.out.println(i);
            if (i.hasExpired()) {
                expiredItems.add(i);
            }
        }
        items.removeAll(expiredItems);
        return expiredItems;
    }
}
